

前提: 
	1、安装eclick插件(将eclick_1.0.0.201501060634.jar插件放入eclipse->plugins然后重启eclipse),否则convert.elick文件不可用; 
	
	2、将此项目导入eclipse，部署到Tomcat或其他支持JSP的服务器; 并且项目必须支持JDK1.6以上,因为其中用到了一个JDK1.6才出现的类
	注意
	    默认项目中很多地方会请求8080端口如果你的服务器端口不是8080而是其它端口,例如是8888则进行如下操作:
	    进行以下两个地方的修改   
	 (1)   
	    "test.ToolMain.java"Java文件中 getNewSource(String basePath)方法中URL请求部分  =>
	   " URLConnectGetTool msg = new URLConnectGetTool("http://localhost:8080/weaver_work/", reps, newsrcMod, jspmod);"
	   为
	   " URLConnectGetTool msg = new URLConnectGetTool("http://localhost:8888/weaver_work/", reps, newsrcMod, jspmod);"
	 
	 (2)   
	    以及"convert.eclick"文件中  =>
	    " <a href="http://localhost:8080/weaver_work/convert.jsp?work=toJsp" target="toJspInfo">old源码->JSP</a> "
	    为
	    " <a href="http://localhost:8888/weaver_work/convert.jsp?work=toJsp" target="toJspInfo">old源码->JSP</a>	 "
	    
	    " <a href="http://localhost:8080/weaver_work/convert.jsp?work=toSrc" target="toJspInfo">JSP->new源码</a> "
	    为
	    " <a href="http://localhost:8888/weaver_work/convert.jsp?work=toSrc" target="toJspInfo">JSP->new源码</a>	 "
	    
3、
本项目进行作业有如下两种方式：

（1）傻瓜式：打开convert.elick并切换到Preview! (先必须安装eclick插件然后重启eclipse)

（2）编程式：请运行 Working->test.ToolMain.java这个类的main方法来直接执行(右键Run as ->Java Application!)



