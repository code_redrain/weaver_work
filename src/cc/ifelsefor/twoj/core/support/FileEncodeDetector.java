package cc.ifelsefor.twoj.core.support;

import java.io.File;

public abstract class FileEncodeDetector {
	/**
	 * 重写此方式以便于以自定义的方式判定文件编码
	 * @param textFile
	 * @return
	 */
	public abstract String getFileEncode(File textFile);
	
}
