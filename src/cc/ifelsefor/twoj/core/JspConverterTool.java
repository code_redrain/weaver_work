package cc.ifelsefor.twoj.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.XPathFactoryConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import cc.ifelsefor.twoj.core.support.FileEncodeDetector;

/**
 *
 * <p>Title: Wang - TransferJsp</p>
 *
 * <p>Description: 将所有模版资源文件夹(srcmod)内， 所有符合过滤器(filter)指定文件的文件， 全部转换成模版JSP文件，并保存在新的模版文件夹(newmod)内.在转换过程中可以替换冲突字符串
 * <p>“<span style="color:red">注意：</span>如果文件已经被转换，即文件后缀为.jsp的新模版文件存在，将不再做转换处理；如果要重复处理请将对应.jsp新模版文件删除，再重新转换”
 * 
 * <p>Copyright: WangHuarong's</p>
 *
 * <p>Company: personal</p>
 *
 * @author wanghr
 * @version 1.0
 */

public class JspConverterTool {
	
	static JspConverterTool conv;
	
	/**
	 * 资源文件夹路径
	 */
	private File srcmod;
	
	/**
	 * 生成JSP文件夹路径,一般直接定位到eclipse模板jsp工程
	 */
	private File jspmod;
	
	/**
	 * 要被转换的文件的类型
	 */
	private FileNameExtensionFilter filter; 
	
	/**
	 * <p>需要被转换的二维数组，例如：{{"<%","<`%"},{"<jsp:","<`jsp:"}};
	 * * 会将<%替换成<`%，将<jsp:替换成<`jsp:
	 */
	private String[][] reps;
	
	/**
	 * JSP头要加入的包
	 */
	private String packageString="";
	
	
	public JspConverterTool(){
		
	}
			
	public File getSrcmod() {
		return srcmod;
	}

	public void setSrcmod(File srcmod) {
		this.srcmod = srcmod;
	}

	public File getJspmod() {
		return jspmod;
	}

	public void setJspmod(File jspmod) {
		this.jspmod = jspmod;
	}

	public FileNameExtensionFilter getFilter() {
		return filter;
	}

	public void setFilter(FileNameExtensionFilter filter) {
		this.filter = filter;
	}

	public String[][] getReps() {
		return reps;
	}

	public void setReps(String[][] reps) {
		this.reps = reps;
	}

	public String getPackageString() {
		return packageString;
	}

	public void setPackageString(String packageString) {
		this.packageString = packageString;
	}
	
	/**
	 * 
	 * @param srcmod： 源模版文件夹目录 <p> 
	 * @param newmod： 生成模版文件夹目录 <p> 
	 * @param fileFilter：将被转换成JSP模版的源文件类型， 例如：new String[]{"jsp","jspx","txt",".js",".java"}
	 * @param reps: 需要被转换的字符和转换字符组列表：
	 * <p>{{"<%","<`%"},{"<jsp:","<`jsp:"}};会将<%替换成<`%，将<jsp:替换成<`jsp:
	 * 
	 */
	public JspConverterTool(File srcmod,File jspmod,String[] fileFilter,String[][] reps){
		this.srcmod = srcmod;
		this.jspmod = jspmod;
		this.filter = new FileNameExtensionFilter(null,fileFilter);
		this.reps 	= reps; 
	}
	
	/**
	 * 需要加入的包名,多个包以","分割
	 * @param splitPackageString
	 */
	public void importPackage(String packageString){
		this.packageString = packageString; 
	}
	
	/**
	 * (1)获取目录所有文件
	 * @throws Exception 
	 */
	public static  List<File> getAllFiles(File file) throws Exception{
		if(!file.exists()){
			throw new Exception(file.getAbsolutePath()+" 目录不存在!");
		}
		if(!file.isDirectory()){
			throw new Exception(file.getAbsolutePath()+" 必须是一个文件夹!");
		}
		List<File> list = new ArrayList<File>();
		getAllFilesInStack(file,list);
		return list;
	}
	
	/**
	 * (2)获取目录所有文件内部栈递归方法
	 * @param file
	 * @param list
	 * @return
	 */
	private static  void getAllFilesInStack(File file,List<File> list){
		File[] listFiles = file.listFiles();
		list.add(file);
		if(listFiles==null||listFiles.length==0){
			return;
		}
		for(int i=0;i<listFiles.length;i++){
			getAllFilesInStack(listFiles[i],list);
		}
		return;
	}
	
	/**
	 * 获取文件编码UTF-8/或操作系统编码,判定方法，如果不是UTF-8编码就是操作系统编码
	 * @param text
	 * @return UTF-8 或 操作系统编码
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	protected String getUTF8OrSysEncode(File text) throws FileNotFoundException, IOException{
        InputStream in= new java.io.FileInputStream(text);
        byte[] b = new byte[in.available()];
        try{
        	in.read(b);
        }catch(Exception e){
        	e.printStackTrace();
        }finally{
        	in.close();
        }
        
        boolean isUtf8 = isUTF8(b);
        
        if (isUtf8){
            System.out.println(text.getName() +"：编码为UTF-8");
        	return "UTF-8";
        }
        else{
            System.out.println(text.getName() +"：可能是"+ System.getProperty("file.encoding"));
    		return System.getProperty("file.encoding");
        }
	}
	
	/**
	 * 参考的判断文件是否是UTF-8编码的方法,如果你觉得有异议,可以重写此方法
	 * @param rawtext
	 * @return
	 */
	protected boolean isUTF8(byte[] rawtext) {
		   int score = 0;
		   int i, rawtextlen = 0;
		   int goodbytes = 0, asciibytes = 0;
		   // Maybe also use UTF8 Byte Order Mark: EF BB BF
		   // Check to see if characters fit into acceptable ranges
		   rawtextlen = rawtext.length;
		   for (i = 0; i < rawtextlen; i++) {
		    if ((rawtext[i] & (byte) 0x7F) == rawtext[i]) {
		     // 最高位是0的ASCII字符
		     asciibytes++;
		     // Ignore ASCII, can throw off count
		    } else if (-64 <= rawtext[i] && rawtext[i] <= -33
		      //-0x40~-0x21
		      && // Two bytes
		      i + 1 < rawtextlen && -128 <= rawtext[i + 1]
		      && rawtext[i + 1] <= -65) {
		     goodbytes += 2;
		     i++;
		    } else if (-32 <= rawtext[i]
		      && rawtext[i] <= -17
		      && // Three bytes
		      i + 2 < rawtextlen && -128 <= rawtext[i + 1]
		      && rawtext[i + 1] <= -65 && -128 <= rawtext[i + 2]
		      && rawtext[i + 2] <= -65) {
		     goodbytes += 3;
		     i += 2;
		    }
		   }
		   if (asciibytes == rawtextlen) {

		    return false;
		   }
		   score = 100 * goodbytes / (rawtextlen - asciibytes);
		   // If not above 98, reduce to zero to prevent coincidental matches
		   // Allows for some (few) bad formed sequences
		   if (score > 98) {
		    return true;
		   } else if (score > 95 && goodbytes > 30) {
		    return true;
		   } else {
		    return false;
		   }
	}
	
	/**
	 * 
	 *  将源文件夹中的文件转换为JSP文件保存到新的输出文件夹中.
	 *  注意:如果输出文件夹已经包含了该对应源文件的JSP文件,那该JSP文件将不会重新生成;
	 *  如果您想重新生成该文件,一个不错的方式是删除或重命名该JSP文件,
	 *  如果您想重新生成全部或部分文件,就相应地改变目录名或者删除这些目录,然后重新执行此方法
	 * @throws Exception 
	 *  
	 */
	public void convertSrcToJsp() throws Exception{
		List<File> files = getAllFiles(srcmod);
		for(File fileo :files){
			if(fileo.isFile()){
				if(filter.accept(fileo)){
					File toNewmodFile = new File(fileo.getAbsolutePath().replace(this.srcmod.getAbsolutePath(),this.jspmod.getAbsolutePath())+".jsp");
					// 未能转换成JSP文件,立即转换:
					if(!toNewmodFile.exists()){
						try {
							transferJSP(fileo,this.getUTF8OrSysEncode(fileo));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}
	
	
	/**
	 *  以特定方式检测文件编码,并以检测出的编码方式输出文本文件
	 *  将源文件夹中的文件转换为JSP文件保存到新的输出文件夹中.
	 *  注意:如果输出文件夹已经包含了该对应源文件的JSP文件,那该JSP文件将不会重新生成;
	 *  如果您想重新生成该文件,一个不错的方式是删除或重命名该JSP文件,
	 *  如果您想重新生成全部或部分文件,就相应地改变目录名或者删除这些目录,然后重新执行此方法 
	 * @param fileEncodeDetector
	 * @throws Exception 
	 */
	public void convertSrcToJsp(FileEncodeDetector fileEncodeDetector) throws Exception{
		List<File> files = getAllFiles(srcmod);
		for(File fileo :files){
			if(fileo.isFile()){
				if(filter.accept(fileo)){
					File toNewmodFile = new File(fileo.getAbsolutePath().replace(this.srcmod.getAbsolutePath(),this.jspmod.getAbsolutePath())+".jsp");
					// 未能转换成JSP文件,立即转换:
					if(!toNewmodFile.exists()){
						try {
							transferJSP(fileo,fileEncodeDetector.getFileEncode(fileo));
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}
	
	/**
	 * 将文件转换成.JSP文件
	 * @throws Exception 
	 */
	private void transferJSP(File file,String fileEncode) throws Exception{
		byte[] data = new byte[(int)file.length()];
		FileInputStream fis = new FileInputStream(file);
		
		//读取源模版文件
		try{
			fis.read(data);
		}catch(Exception e){
			throw e;
		}finally{
			fis.close();
		}
		
		//获取UTF-8或GBK文件编码,如果文件不是这两种编码将不能保证正确进行继续的操作
		String encodeFileStr = new String(data,fileEncode);
		
		
		//未经转换，立即转换
			/*--------------------------start:----------------------------*/
			// 构造头
			String head = "<%@page language=\"java\" import=\"java.util.*"+(packageString!=null&&!packageString.equals("")?","+packageString:"")+"\" pageEncoding=\""+ fileEncode + "\"%>\n";
			
			// 构造分割
			String headsplit = "<!-- -----------------------headsplit---------------------- -->\n";			
			
			// 转换指定字符,使得java代码标签输出成文本,而不会被tomcat转换成java代码
			for(int i=0; i<this.reps.length; i++){
				encodeFileStr = encodeFileStr.replace(reps[i][0], reps[i][1]);
			}
			
			// 构造新文件内容
			String newStr = head + headsplit + encodeFileStr;
			
			// 构造生成文件路径
			File newmodFile = new File(file.getAbsolutePath().replace(srcmod.getAbsolutePath(), jspmod.getAbsolutePath())+".jsp");
			
			// 构造新文件的目录链
			newmodFile.getParentFile().mkdirs();
			
			// 构造新文件输出流
			FileOutputStream jspmodFileo = new FileOutputStream(newmodFile);
			
			// 输出新模版文件以原编码方式
			try{
				jspmodFileo.write(newStr.getBytes(fileEncode));
			}catch(Exception e){
				throw e;
			}finally{
				jspmodFileo.close();
			}
			/*---------------------------- end ------------------------------*/
	}
	
	/**
	 * 
	 * @param xmlPath : 根据实例配置文件路径获取实例
	 * @return
	 * @throws Exception 
	 */
	public  static JspConverterTool getInstance(String xmlFilePath) throws Exception {
			if(conv!=null){
				return conv;
			}
			if(!new File(xmlFilePath).exists()){
				throw new Exception("文件不存在!");
			}
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	        dbf.setValidating(false);
	        DocumentBuilder db = dbf.newDocumentBuilder();
	        Document doc = db.parse(xmlFilePath);
	        
		   return getInstance(doc);
	}
	
	/**
	 * 
	 * @param ins 根据配置文件流获取实例
	 * @return
	 * @throws FileNotFoundException
	 * @throws SAXException
	 * @throws IOException
	 * @throws ParserConfigurationException
	 * @throws XPathFactoryConfigurationException
	 * @throws XPathExpressionException
	 */
	public  static JspConverterTool getInstance(InputStream ins) throws FileNotFoundException, SAXException, IOException, ParserConfigurationException, XPathFactoryConfigurationException, XPathExpressionException {
		if(conv!=null){
			return conv;
		}
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setValidating(false);
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(ins);
        
	   return getInstance(doc);
	}
	
	/**
	 *  根据文档对象获取实例
	 * @param docs
	 * @return
	 * @throws XPathExpressionException
	 */
	private static JspConverterTool getInstance(Document doc) throws XPathExpressionException{
		XPathFactory factory = XPathFactory.newInstance();
        XPath xpath = factory.newXPath();
        Node srcmodNode = (Node)xpath.evaluate("instance/srcmod", doc, XPathConstants.NODE);
        Node jspmodNode = (Node)xpath.evaluate("instance/jspmod", doc, XPathConstants.NODE);
        Node filterNode = (Node)xpath.evaluate("instance/filter", doc, XPathConstants.NODE);
        NodeList repsNodeList = (NodeList)xpath.evaluate("instance/reps/rep", doc, XPathConstants.NODESET);
        File srcmod = new File(srcmodNode.getTextContent().replaceAll("[\\r\\n]","").replaceAll("\\s", " ").trim());
        File jspmod = new File(jspmodNode.getTextContent().replaceAll("[\\r\\n]","").replaceAll("\\s", " ").trim());
        String[] filter = filterNode.getTextContent().split(",");
        String[][] reps = new String[repsNodeList.getLength()][2];
        for(int i=0;i<repsNodeList.getLength();i++){
        	Node rep0 = null;
        	Node rep1 = null;
        	NodeList nodeList = repsNodeList.item(i).getChildNodes();
        	for(int j=0;j<nodeList.getLength();j++){
        		if(nodeList.item(j).getNodeName().equals("rep0")){
        			rep0 = nodeList.item(j);
        		}
        		if(nodeList.item(j).getNodeName().equals("rep1")){
        			rep1 = nodeList.item(j);
        		}
        	}
        	reps[i][0] = rep0.getTextContent().trim();
        	reps[i][1] = rep1.getTextContent().trim();
        }
        
        return conv = new JspConverterTool(srcmod, jspmod, filter, reps);
	}
	
	
	/**
	 * 获取默认实例(根据jar包中的默认配置)
	 * @param sources 资源文件夹
	 * @throws ParserConfigurationException 
	 * @throws IOException 
	 * @throws SAXException 
	 * @throws XPathExpressionException 
	 * @throws XPathFactoryConfigurationException 
	 * @throws FileNotFoundException 
	 */
	public static JspConverterTool getInstanceDefault(File sources) throws Exception{
		InputStream ins = JspConverterTool.class.getResourceAsStream("/config/default.xml");
		getInstance(ins).setSrcmod(sources);
		if(sources.isDirectory()){
			getInstance(ins).setJspmod(new File(sources.getAbsolutePath()+"_jsp"));
		}
		
		return getInstance(ins);
	}
	
	/**
	 * 获取默认实例(根据jar包中的默认配置)
	 * @param sourcesPath 资源文件夹路径
	 * @throws ParserConfigurationException 
	 * @throws IOException 
	 * @throws SAXException 
	 * @throws XPathExpressionException 
	 * @throws XPathFactoryConfigurationException 
	 * @throws FileNotFoundException 
	 */
	public static JspConverterTool getInstanceDefault(String sourcesPath) throws Exception{
		if(sourcesPath==null||"".equals(sourcesPath)){
			throw new Exception("不能指定空路径!");
		}
		File sources = new File(sourcesPath);
		InputStream ins = JspConverterTool.class.getResourceAsStream("/config/default.xml");
		getInstance(ins).setSrcmod(new File(sourcesPath));
		getInstance(ins).setJspmod(new File(sources.getAbsolutePath()+"_jsp"));
		
		return getInstance(ins);
	}
	
	public static void main(String[] args) throws Exception{
		//JspConverter.getInstanceByXml("asd.xml");
		//System.out.println(new InputSource("config/convert_instance_demo.xml"));
		//  test();
		//JspConverter config = JspConverter.getInstance("config\\myconfige.xml");
		// config.convertSrcToJsp();
	}

	
}
