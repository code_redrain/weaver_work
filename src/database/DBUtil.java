package database;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
public class DBUtil {

	static class Ora{
		static final String DRIVER_CLASS = "oracle.jdbc.driver.OracleDriver";
		static final String DATABASE_URL = "jdbc:oracle:thin:@localhost:1521:orcl";
		static final String DATABASE_USER = "dev";
		static final String DATABASE_PASSWORD = "dev";
	}

	static class MySql{
		static final String DRIVER_CLASS = "com.mysql.jdbc.Driver";
		static final String DATABASE_URL = "jdbc:mysql://localhost/plusoft_test?useUnicode=true&characterEncoding=GBK";
		static final String DATABASE_USER = "root";
		static final String DATABASE_PASSWORD = "1234";
	}

	private static Connection con = null;

	public static Connection getOracleConnection() {

		try {
			Class.forName(Ora.DRIVER_CLASS);
			con=DriverManager.getConnection(Ora.DATABASE_URL,Ora.DATABASE_USER,Ora.DATABASE_PASSWORD);
			return con;
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
		return con;
	}

	public static Connection getMySqlConnection() {
		try {
			Class.forName(MySql.DRIVER_CLASS);
			con=DriverManager.getConnection(MySql.DATABASE_URL,MySql.DATABASE_USER,MySql.DATABASE_PASSWORD);
			return con;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return con;
	}


	/***
	 * 打印MySql的表模板参数文件(jsp):
	 * @throws SQLException
	 * @throws UnsupportedEncodingException
	 */
	public static void sysoutMySqlTCloumns(String Table) throws SQLException, UnsupportedEncodingException{
		getMySqlConnection();
		//System.setProperty("file.encoding", "UTF-8");
		List<HashMap<String,String>> columns = new ArrayList<HashMap<String,String>>();
		try{
			Statement stmt = con.createStatement();
			String sql = " show full columns from "+Table;
			System.out.println(sql);
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()){
				HashMap<String,String> map = new HashMap<String,String>();
				String Comment = rs.getString("Comment");
				map.put("Name",Comment);
				map.put("Code", rs.getString("Field"));
				map.put("DataType", rs.getString("Type"));
				map.put("Comment", rs.getString("Comment")); //varchar int text datetime
				map.put("Primary", "YES".equals(rs.getString("Key"))?"TRUE":"FALSE");
				map.put("Mandatory", !"YES".equals(rs.getString("Null"))?"TRUE":"FALSE");
				columns.add(map);
			}

		}
		catch (SQLException e){
			e.printStackTrace();
		}finally{
			con.close();
		}

		// 输出
		for(HashMap<String,String> map : columns){
			String Name = map.get("Name");
			String Code = map.get("Code");
			String Comment = map.get("Comment");
			String DataType = map.get("DataType");
			String Primary = map.get("Primary");
			Name = Comment.split("\\s+")[0];
			String Mandatory = map.get("Mandatory");
			String str = "table.cols.add(new Column(\""+Name+"\",\""+Code+"\",\""+Comment+"\",\""+DataType+"\",\""+Primary+"\",\""+Mandatory+"\"));";
			System.out.println(str);
		}
	}


	/***
	 * 打印ORACLE的表模板参数文件(jsp):
	 * @throws SQLException
	 */
	public static void sysoutOracleTCloumns(String Table,String Owner) throws SQLException{
		getOracleConnection();
		List<HashMap<String,String>> columns = new ArrayList<HashMap<String,String>>();
		try{
			Statement stmt = con.createStatement();
			String sql=
			 "select "+
			 "	       comments as \"Name\","+
			 "	       a.column_name \"Code\","+
			 "	       a.DATA_TYPE as \"DataType\","+
			 "	       b.comments as \"Comment\","+
			 "	       decode(c.column_name,null,'FALSE','TRUE') as \"Primary\","+
			 "	       decode(a.NULLABLE,'N','TRUE','Y','FALSE','') as \"Mandatory\","+
			 "	       '' \"sequence\""+
			 "	 from "+
			 "	     all_tab_columns a, "+
			 "	     all_col_comments b,"+
			 "	     ("+
			 "	      select a.constraint_name, a.column_name"+
			 "	        from user_cons_columns a, user_constraints b"+
			 "	       where a.constraint_name = b.constraint_name"+
			 "	             and b.constraint_type = 'P'"+
			 "	             and a.table_name = '"+Table+"'"+
			 "	     ) c"+
			 "	 where "+
			 "	   a.Table_Name=b.table_Name "+
			 "	   and a.column_name=b.column_name"+
			 "	   and a.Table_Name='"+Table+"'"+
			 "	   and a.owner=b.owner "+
			 "	   and a.owner='"+Owner+"'"+
			 "	   and a.COLUMN_NAME = c.column_name(+)" +
			 "	order by a.COLUMN_ID";
			System.out.println(sql);
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()){
				HashMap<String,String> map = new HashMap<String,String>();
				map.put("Name", rs.getString("Name"));
				map.put("Code", rs.getString("Code"));
				map.put("DataType", rs.getString("DataType"));
				map.put("Comment", rs.getString("Comment"));
				map.put("Primary", rs.getString("Primary"));
				map.put("Mandatory", rs.getString("Mandatory"));
				columns.add(map);
			}
		}
		catch (SQLException e){
			e.printStackTrace();
		}finally{
			con.close();
		}

		// 输出
		for(HashMap<String,String> map : columns){
			String Name = map.get("Name");
			String Code = map.get("Code");
			String Comment = map.get("Comment");
			String DataType = map.get("DataType");
			String Primary = map.get("Primary");
			Name = Comment.split("\\s+")[0];
			String Mandatory = map.get("Mandatory");
			String sequence = map.get("sequence");
			String str = "table.cols.add(new Column(\""+Name+"\",\""+Code+"\",\""+Comment+"\",\""+DataType+"\",\""+Primary+"\",\""+Mandatory+"\",\""+(sequence==null?"":sequence)+"\"));";
			System.out.println(str);
		}
	}


	public static void main(String[] args) throws SQLException, UnsupportedEncodingException{

		sysoutMySqlTCloumns("t_student");


		// 甲供材
		// sysoutStrTablePdmCloumns("CT_INFO_STUFF_TYPE","DEV");
		//   sysoutStrTablePdmCloumns("CT_INFO_STUFF_TYPE_KPI","DEV");
		// 印花税目
		//sysoutStrTablePdmCloumns("CT_INFO_STAMP_TAX","DEV");
		// 结算方式
		//sysoutStrTablePdmCloumns("SYS_STD_CODE_VAL","DEV");
		// 项目标段
		//sysoutStrTablePdmCloumns("CT_INFO_PROJECT_TENDERS","DEV");
		// 甲供材产品
		//sysoutStrTablePdmCloumns("CT_INFO_STUFF","DEV");
		// 甲供材指标
		//sysoutStrTablePdmCloumns("CT_INFO_STUFF_KPI","DEV");
		//产品报价
		//sysoutStrTablePdmCloumns("CT_INFO_STUFF_OFFER","DEV");
		//甲供材预算
		//sysoutStrTablePdmCloumns("CT_INFO_STUFF_BUDGET","DEV");
		// 合同借阅
		//采购订单
		//  sysoutStrTablePdmCloumns("CT_INFO_STUFF_ORDER","DEV");
		//采购订单明细
		//  sysoutStrTablePdmCloumns("CT_INFO_STUFF_ORDER_ITEM","DEV");

		//单价对比方案
		//  sysoutStrTablePdmCloumns("CT_INFO_STUFF_PRICE_PLAN","DEV");
		//材料单价对比明细
		//  sysoutStrTablePdmCloumns("CT_INFO_STUFF_PRICE_COMPARE","DEV");
		//材料供货单
		//  sysoutStrTablePdmCloumns("CT_INFO_STUFF_DELIVERY","DEV");
		//材料供货单材料列表
		//  sysoutStrTablePdmCloumns("CT_INFO_STUFF_DELIVERY_ITEM","DEV");
		/* 菜单查询  */
		//sysmenuandfunc("甲供材预算","erp_cost_jgcgl_stuffbudget","erp_cost_jgcgl_stuffbudget_List", "10.03.03.01", "10.03.03", "1");
		//sysoutStrTablePdmCloumns("CT_INFO_STUFF_APPLY","DEV");//材料申请
		//sysoutStrTablePdmCloumns("CT_INFO_STUFF_APPLY_ITEM","DEV");//材料明细
		//sysoutStrTablePdmCloumns("CT_INFO_STUFF_PLAN","DEV");//材料采购计划
		//sysoutStrTablePdmCloumns("CT_INFO_STUFF_PLAN_ITEM","DEV");//材料明细

		//材料计划menu
		// sysmenu("材料计划","erp_cost_jgcgl_stuffplan","erp_cost_jgcgl_stuffplan_Frame", "10.03.03.02", "10.03.03", "2");
		//材料计划-材料申请func
		// sysfunc("申请","erp_cost_jgcgl_stuffplan_apply_item", "10.03.03.02.01", "10.03.03.02", "1");
		//材料计划-材料采购计划func
		// sysfunc("采购","erp_cost_jgcgl_stuffplan_plan", "10.03.03.02.02", "10.03.03.02", "2");

		// ct_info_stuff_order

	}


}


