package database.area;

public class Column {

	//1列中文名
	public String Name = "";
	
	//2列名
	public String Code = "";
	
	//3数据类型
	public String DataType="";
	
	//4注释
	public String Comment=""; 
	
	//5是否主键
	public String Primary="";
	
	//6是否允许null
	public String Mandatory="";
	
	public Column(){
		
	}
	
	public Column(String Name,String Code,String Comment,String DataType,String Primary,String Mandatory){
		this.Name = Name;
		this.Code = Code.toLowerCase();
		this.DataType = DataType;
		this.Primary = Primary;
		this.Comment = Comment;
		this.Mandatory = Mandatory;
	}
	
	
	
	public static void main(String[] args){
		
	}
	
	
}
